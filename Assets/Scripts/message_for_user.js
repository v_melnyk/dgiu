﻿#pragma strict

private var timer: float;
private var showTime: float;
private var activeTimer: boolean;
private var message: String;
private var uiText: UI.Text;
public var soundCollectedObject: AudioClip;
var audi: AudioSource;

function startTimer()
{
    timer = 0.0f;
    showTime = 3.0f;
    uiText.text = message;
    activeTimer = true;
}

function Start()
{
    uiText = GetComponent(UI.Text);
    audi = GetComponent.<AudioSource>();
    audi.clip = soundCollectedObject;
}

function Update()
{
    if (activeTimer)
    {
        timer += Time.deltaTime;
        if (timer > showTime)
        {
            activeTimer = false;
            uiText.text = "";
        }
    }
}
function showText(m: String)
{
	audi.Play();
    message = m;
    startTimer();
}
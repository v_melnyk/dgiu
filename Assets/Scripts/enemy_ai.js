﻿#pragma strict

public var walkSpeed: float = 5.0;
public var attackDistance: float = 1.5;
public var attackRange: float = 10.0;
public var attackDelay: float = 1.0;
public var enemyHealth: float = 10.0;
private var timer: float = 0;

function Start () {

}

function Update () {

}

function Hit()
{
	Destroy(gameObject);
}

function OnTriggerStay(o: Collider)
{
	if(o.tag == "Player"){
		var rotationTarget = Quaternion.LookRotation(o.transform.position - transform.position);
		var originalX = transform.rotation.x;
		var originalZ = transform.rotation.z;
		var finalRotation = Quaternion.Slerp(transform.rotation, rotationTarget, 5.0 * Time.deltaTime);
		finalRotation.x = originalX;
		finalRotation.z = originalZ;
		transform.rotation = finalRotation;
		var distance = Vector3.Distance(transform.position, o.transform.position);
		if(distance > attackDistance)
		{
			transform.Translate(Vector3.forward *walkSpeed * Time.deltaTime);
		}
		else
		{
			if(timer <= 0)
			{
				//o.SendMessage("Hit", attackRange);
				o.gameObject.GetComponent(detect_collision).health -= 10;
				timer = attackDelay;
			}
		}
		if(timer > 0)
		{
			timer -= Time.deltaTime;
		}
	}
}
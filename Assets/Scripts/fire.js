﻿#pragma strict

public var bullets: int;
public var sound: AudioClip;
var audio1: AudioSource;
public var sparkle: GameObject;
public var hole: GameObject;

function Start () {
Cursor.visible = false;
audio1 = GetComponent.<AudioSource>();
audio1.clip = sound;
}

function Update () {
	if (Input.GetButtonDown("Fire1") && bullets > 0)
	{
		audio1.Play();
		bullets -= 1;
		var hit : RaycastHit;
		var angle = Camera.main.ScreenPointToRay(Vector3(Screen.width/2,Screen.height/2));

		var textToDisplay:String = "ammo: " + bullets;
		GameObject.Find("UI_bullets").GetComponent(UI.Text).text = textToDisplay;

		if (GameObject.Find("UI_bullets").GetComponent(fire).bullets == 0) {
			GameObject.Find("UI_texture_gun").GetComponent(UI.RawImage).enabled = false;
    		GameObject.Find("UI_texture_sight").GetComponent(UI.RawImage).enabled = false;
    		GameObject.Find("UI_bullets").GetComponent(UI.Text).text = "";
		}
	}

	if(Physics.Raycast(angle, hit, 100))
	{
		Instantiate(sparkle, hit.point,Quaternion.identity);
		//print("You hit in " + hit.collider.gameObject.tag);
		if (hit.collider.gameObject.tag == "enemy" && hit.collider.GetType().ToString() == "UnityEngine.BoxCollider"){
			Destroy(hit.collider.gameObject);
			//print(hit.collider.GetType().ToString());
		} else {
			Instantiate(hole, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
		}
	}
}
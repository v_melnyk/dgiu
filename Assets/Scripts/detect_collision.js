﻿#pragma strict

public var health: int;
public var isGun: boolean;
private var isKey: boolean;
private var isMatches: boolean;
private var ls: UnityEngine.SceneManagement.SceneManager;
var player: GameObject;
public var smoke: ParticleSystem;
public var camp_fire: ParticleSystem;

public var sound_death: AudioClip;
var audio_death: AudioSource;

//private var showHint: boolean = false;
//private var timer: float = 0;
//public var displayTime: float = 5.0f;
//public var hintGUI: UI.Text;

function Start () {
    health = 50;
    isGun = false;
    isKey = false;
    isMatches = false;
    changeTexture("key", isKey);
    changeTexture("gun", isGun);
    changeTexture("sight", isGun);
    changeTexture("matches", isMatches);

    audio_death = GetComponent.<AudioSource>();
	audio_death.clip = sound_death;
}

function Update () {
	if(player.transform.position.y < -4){
		die();
	}
	//if(showHint)
	//{
	//	if(timer < displayTime)
	//	{
	//		timer += Time.deltaTime;
	//	} else {
	//		hintGUI.enabled = false;
	//		showHint = false;
	//		timer = 0;
	//	}
	//}

   	if (health <= 0) {
   		die();
   	}
   	GameObject.Find("health_bar").GetComponent(health_bar).setHealth(health);
   	if (Input.GetKeyDown(KeyCode.Escape)) {
        Application.Quit();
    }
}

function OnControllerColliderHit(c : ControllerColliderHit){

    if (c.gameObject.tag == "firstAidKit"){
        Destroy(c.gameObject);
        health = 100;
        GameObject.Find("health_bar").GetComponent(health_bar).setHealth(health);
        GameObject.Find("UI_message_for_user").GetComponent(message_for_user).showText(c.gameObject.tag + " collected!");
        GameObject.Find("Fall_trap").GetComponent(top_trap).activate();
    } else if (c.gameObject.tag == "gun"){
        Destroy(c.gameObject);
        isGun = true;
        changeTexture("gun", isGun);
        changeTexture("sight", isGun);
        GameObject.Find("UI_message_for_user").GetComponent(message_for_user).showText(c.gameObject.tag + " collected!");
        GameObject.Find("UI_bullets").GetComponent(fire).bullets = 20;
		GameObject.Find("UI_bullets").GetComponent(UI.Text).text = "ammo: 20";
    } else if (c.gameObject.tag == "key"){
        Destroy(c.gameObject);
        isKey = true;
        changeTexture("key", isKey);
        GameObject.Find("UI_message_for_user").GetComponent(message_for_user).showText(c.gameObject.tag + " collected!");
    } else if (c.gameObject.tag == "matches"){
        Destroy(c.gameObject);
        isMatches = true;
        changeTexture("matches", isMatches);
        GameObject.Find("UI_message_for_user").GetComponent(message_for_user).showText(c.gameObject.tag + " collected!");
    } else if (c.gameObject.tag == "door"){
    	if (isKey == true){
    		GameObject.Find("UI_message_for_user").GetComponent(message_for_user).showText("Level finished!");
    		ls.LoadScene("scene01");
    	}
        
    } else if (c.gameObject.tag == "sphere")
    {
        die();
    } else if (c.gameObject.tag == "trap_door"){
        //yield WaitForSeconds(2);
        c.gameObject.GetComponent(push_trap).activate();

    } else if (c.gameObject.tag == "Fall_trap"){
        die();

    } else if (c.gameObject.tag == "fire"){
       	if (isMatches == true){
       		GameObject.Find("UI_message_for_user").GetComponent(message_for_user).showText("Fire lit!");
       		GameObject.Find("campfire").GetComponent(camp_fire_script).fire_audio();
       		camp_fire.Play();
       		smoke.Play();
       		isMatches = false;
        	changeTexture("matches", isMatches);
       	}
    } else if (c.gameObject.tag == "enemy"){
   		
    }
}

function changeTexture(object: String, show: boolean)
{
     GameObject.Find("UI_texture_" + object).GetComponent(UI.RawImage).enabled = show;
}

//function showHintGUI(txt: String)
//{
//	hintGUI.text = txt;
//	hintGUI.enabled = true;
//	this.showHint = true;
//}

function die() {
	audio_death.Play();
	GameObject.Find("UI_message_for_user").GetComponent(message_for_user).showText("GAME OVER!");
    yield WaitForSeconds(1);
	Application.LoadLevel(Application.loadedLevel);
}
﻿#pragma strict

public var health : int = 50;
private var currentColor: Texture2D;
public var style: GUIStyle;
public var textureGreen: Texture2D;
public var textureYellow: Texture2D;
public var textureRed: Texture2D;
public var textureBlack: Texture2D;

function Start () {

}

function Update () {

}

function OnGUI()
{
	if (health >= 67){
		currentColor = textureGreen;

	} else if (health >= 34) {
		currentColor = textureYellow;
	} else {
		currentColor = textureRed;
	}
	style.normal.background = textureBlack;
	GUI.Box(Rect(0, 25, 100, 20), "", style);
	style.normal.background = currentColor;
	GUI.Box(Rect(0, 25, health, 20), "", style);
}

public function setHealth(value: int)
{
	health = value;
}